/*
30002105
Eli Fawcett
14/06/18
*/
/*
The purpose of this program is to be able to enter the vans details and give you a total cost at the end.
It is usefull because you could enter more than just one set of variables so it allows the company to be able to have more than just 1 van.
At the end it'll give the user a total cost
*/
let TotalCost=0;
let VanID="";
let Capacity=0;
let License="";
let CostPerDay=0;
let Insurance=0;
//Class getting and setting the different variables. Has constructor.
console.log(`EZ Van hire: Van Details \n   `)
class Van {
    get VanID() {
        return this._vanID
    }
    set VanID(value) {
        this._vanID = value
    }
    get Capacity() {
        return this._capacity
    }
    set Capacity(value) {
        this._capacity = value
    }
    get License() {
        return this._license
    }
    set License(value) {
        this._license = value
    }
    get CostPerDay() {
        return this._costPerDay
    }
    set CostPerDay(value) {
        this._costPerDay = value
    } 
    get Insurance() {
        return this._insurance
    }
    set Insurance(value) {
        this._insurance = value
    }
    
    constructor(vanID, capacity, license, costPerDay, insurance){
    this.VanID = vanID;
    this.capacity = Capacity;
    this.license = License;
    this.costPerDay = CostPerDay;
    this.insurance = Insurance;
}}

//Making ClassVan = to the class we made above
let ClassVan = new Van()

//Prompting for the variables and displaying the given inputs in the console.
Van.VanID=prompt(`EZ Van Hire: Enter Van Details. \n \n Van ID`)
console.log(`Vans ID code: ${Van.VanID}`)

Van.Capacity=Number(prompt(`EZ Van Hire: Enter Van Details. \n \n Load Capacity (m3):`))
console.log(`Vans Capacity: ${Van.Capacity}`)

Van.License=prompt(`EZ Van Hire: Enter Van Details. \n \n License type:`)
console.log(`License Type: ${Van.License}`)

Van.CostPerDay=Number(prompt(`EZ Van Hire: Enter Van Details. \n \n Hire Cost Per Day $:`))
console.log(`Vans cost per day: $${Van.CostPerDay}`)

Van.Insurance=Number(prompt(`EZ Van Hire: Enter Van Details. \n \n Insurance Per Day $:`))
console.log(`Insurance cost daily: $${Van.Insurance}`)

//Working out the total cost of the van including insurance and costperday
TotalCost= Van.CostPerDay + Van.Insurance
console.log(`Total cost of the van including insurance = $${TotalCost}`)